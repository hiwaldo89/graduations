<?php 
    // $graduates = get_users(array(
    //     'role' => 'graduate'
    // ));
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mb-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Teléfono</th>
                                <th>Correo</th>
                                <th>No. boletos</th>
                                <th>No. de mesa</th>
                                <th>Total</th>
                                <th>Menú especial</th>
                                <th>Pagos</th>
                                <th>Deuda</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($graduates as $graduate) : ?>
                                <?php 
                                    $eventCode = get_user_meta($graduate->ID, 'event_code', true);
                                    $event = get_posts(array(
                                        'numberposts'	=> -1,
                                        'post_type'		=> 'event',
                                        'meta_key'		=> 'codigo_del_evento',
                                        'meta_value'	=> $eventCode
                                    ));
                                    $payments = get_posts(array(
                                        'post_type' => 'payment',
                                        'meta_query' => array(
                                            array(
                                                'key' => 'user_id',
                                                'value' => $graduate->ID
                                            ),
                                            array(
                                                'key' => 'event_code',
                                                'value' => $eventCode
                                            )
                                        )
                                    ));
                                ?>
                                <tr>
                                    <th><?php echo $graduate->user_firstname . ' ' . $graduate->user_lastname; ?></th>
                                    <th><?php echo get_user_meta($graduate->ID, 'phone', true); ?></th>
                                    <th><?php echo $graduate->user_email; ?></th>
                                    <th>
                                        <?php 
                                            if(get_user_meta($graduate->ID, 'tickets', true)){
                                                echo get_user_meta($graduate->ID, 'tickets', true);
                                            } else {
                                                echo 'N/A';
                                            }
                                        ?>
                                    </th>
                                    <th>
                                        <?php
                                            if(get_user_meta($graduate->ID, 'table_number', true)) {
                                                echo get_user_meta($graduate->ID, 'table_number', true);
                                            } else {
                                                echo 'N/A';
                                            }
                                        ?>
                                    </th>
                                    <th>
                                        <?php echo '$' . (money_format('%n', (double)get_user_meta($graduate->ID, 'total', true)) ?: '0'); ?>
                                    </th>
                                    <th>
                                        <?php 
                                            $graduateSpecialMenus = get_field_object('menu_especial', 'user_' . $graduate->ID);
                                            if($graduateSpecialMenus) {
                                                foreach($graduateSpecialMenus['sub_fields'] as $specialMenu) {
                                                    echo $specialMenu['label'] . ': ' . $graduateSpecialMenus['value'][$specialMenu['name']] . '<br>';  
                                                }
                                            } else {
                                                echo 'N/A';
                                            }
                                        ?>
                                    </th>
                                    <th>
                                        <!-- 31/03/19 - $999 -->
                                        <?php 
                                            if($payments) {
                                                foreach($payments as $payment) {
                                                    echo date('d/m/y', strtotime($payment->post_date)) . ' - $' . get_field('quantity', $payment->ID) . '<br>';
                                                }
                                            } else {
                                                echo 'N/A';
                                            }
                                        ?>
                                    </th>
                                    <th>
                                        <?php echo '$' . (money_format('%n', (double)get_user_meta($graduate->ID, 'remaining', true)) ?: '0'); ?>
                                    </th>
                                    <th>
                                        <a href="<?php echo get_permalink( get_page_by_path('payments/new')) . '?user_id=' . $graduate->ID . '&payment_event_code=' . $eventCode; ?>"><i class="fal fa-usd-circle"></i></a>
                                        <a href="<?php echo get_permalink( get_page_by_path('graduates/edit' )) . '?user_id=' . $graduate->ID; ?>"><i class="fal fa-edit"></i></a>
                                        <a href="#"><i class="fal fa-trash-alt"></i></a>
                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>