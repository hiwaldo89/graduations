<div class="sidebar">
    <?php
        if(is_user_logged_in()) {
            if(current_user_can('app_admin')) {
                wp_nav_menu(array(
                    'theme_location' => 'menu-1',
                    'container' => false,
                    'menu_class' => 'sidebar-menu'
                ));
            } else if(current_user_can('sales_agent')) {
                wp_nav_menu(array(
                    'theme_location' => 'menu-2',
                    'container' => false,
                    'menu_class' => 'sidebar-menu'
                ));
            }
        }
    ?>
</div>