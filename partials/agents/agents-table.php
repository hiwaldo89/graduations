<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mb-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $agents = get_users(array(
                                    'role' => 'sales_agent'
                                ));
                            ?>
                            <?php foreach($agents as $agent) : ?>
                                <tr id="agent-<?php echo $agent->ID; ?>">
                                    <th><?php echo $agent->first_name . ' ' . $agent->last_name; ?></th>
                                    <th><?php echo $agent->user_email; ?></th>
                                    <th>El teléfono del usuario</th>
                                    <th>
                                        <a href="<?php echo get_permalink( get_page_by_path('users/edit' )) . '?user_id=' . $agent->ID; ?>"><i class="fal fa-edit"></i></a>
                                        <a href="#" role="button" data-toggle="modal" data-target="#confirmation-modal" data-user-id="<?php echo $agent->ID; ?>"><i class="fal fa-trash-alt"></i></a>
                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>