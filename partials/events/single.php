<div class="single-event">
    <div class="row mb-4">
        <div class="col-lg-7">
            <?php the_field('description'); ?>
            <p>
                <strong>No. PAX:</strong> <?php echo count($graduates); ?>
            </p>
            <p>
                <strong>Precio del boleto:</strong> $<?php the_field('precio_del_boleto'); ?>
            </p>
            <p>
                <strong>Fecha:</strong> <?php the_field('fecha'); ?>
            </p>
        </div>
        <div class="col-lg-5">
            <?php $layout = get_field('layout'); ?>
            <?php if($layout) : ?>
                <img src="<?php echo $layout['url']; ?>" alt="<?php echo $layout['alt']; ?>" class="img-fluid">
            <?php endif; ?>
        </div>
    </div>
</div>