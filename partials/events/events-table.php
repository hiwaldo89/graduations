<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mb-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Escuela</th>
                                <th>Fecha</th>
                                <th>Venue</th>
                                <th>URL</th>
                                <th>Saldo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $events = new WP_Query(array(
                                    'post_type' => 'event',
                                    'posts_per_page' => 20
                                ));
                            ?>
                            <?php if($events->have_posts()) : while($events->have_posts()) : $events->the_post(); ?>
                                <tr>
                                    <th><?php the_title(); ?></th>
                                    <th><?php the_field('escuela'); ?></th>
                                    <th><?php the_field('fecha'); ?></th>
                                    <th>
                                        <?php $venue = get_field('venue'); ?>
                                        <?php echo $venue->post_title; ?>
                                    </th>
                                    <th>
                                        <?php 
                                            $eventUrl = get_permalink( get_page_by_path('register')) . '?code=' . get_field('codigo_del_evento'); 
                                        ?>
                                        
                                        <a href="<?php echo $eventUrl; ?>"><?php echo $eventUrl; ?></a>
                                    </th>
                                    <th>
                                        <?php echo '$' . money_format('%.2n', get_field('balance')); ?>
                                    </th>
                                    <th>
                                        <a href="<?php the_permalink(); ?>"><i class="fal fa-eye"></i></a>
                                        <a href="<?php echo get_permalink( get_page_by_path('events/edit' )) . '?event_id=' . get_the_ID(); ?>"><i class="fal fa-edit"></i></a>
                                        <a href="#" role="button" data-toggle="modal" data-target="#confirmation-modal" data-delete-link="<?php echo get_delete_post_link(get_the_ID(), '', true); ?>"><i class="fal fa-trash-alt"></i></a>
                                    </th>
                                </tr>
                            <?php endwhile; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>