<div class="payment-detail">
    <?php $graduate = get_user_by('id', $_GET['user_id']); ?>
    <h2 class="h4">Registro de pago para: <?php echo $graduate->user_firstname . ' ' . $graduate->user_lastname; ?></h2>
    <p class="h5">
        <strong>
            Saldo: <?php echo '$' . get_user_meta($graduate->ID, 'remaining', true); ?>
        </strong>
    </p>
</div>
