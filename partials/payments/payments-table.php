<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mb-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Evento</th>
                                <th>Cantidad</th>
                                <th>Agente</th>
                                <th>Fecha</th>
                                <th class="text-right">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if(current_user_can('app_admin')) {
                                    $payments = new WP_Query(array(
                                        'post_type' => 'payment',
                                        'posts_per_page' => 20
                                    ));
                                } else {
                                    $payments = new WP_Query(array(
                                        'post_type' => 'payment',
                                        'posts_per_page' => 20,
                                        'author' => get_current_user_id()
                                    ));
                                }
                            ?>
                            <?php if($payments->have_posts()) : while($payments->have_posts()) : $payments->the_post(); ?>
                                <?php 
                                    $eventCode = get_field('event_code');
                                    $event = get_posts(array(
                                        'numberposts'	=> -1,
                                        'post_type'		=> 'event',
                                        'meta_key'		=> 'codigo_del_evento',
                                        'meta_value'	=> $eventCode
                                    ));
                                    $agent = get_userdata($post->post_author);
                                ?>
                                <tr>
                                    <th><?php the_title(); ?></th>
                                    <th><?php echo $event[0]->post_title; ?></th>
                                    <th>$<?php echo money_format('%.2n', get_field('quantity')); ?></th>
                                    <th><?php echo $agent->first_name . ' ' . $agent->last_name; ?></th>
                                    <th><?php the_time('d/m/Y'); ?></th>
                                    <th class="text-right">
                                        <a href="#" data-delete-link="<?php echo get_delete_post_link(get_the_ID(), '', true); ?>"><i class="fal fa-trash-alt"></i></a>
                                    </th>
                                </tr>
                            <?php endwhile; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>