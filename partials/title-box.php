<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-6">
            <h1 class="h4 page-title"><?php the_title(); ?></h1>
        </div>
        <div class="col-6 text-right">
            <?php if(is_page('events') || is_page('venues') || is_page('graduates') || is_page('users')) : ?>
                <?php 
                    if(is_page('events')) {
                        $newUrl = get_permalink( get_page_by_path('events/new'));
                    } elseif(is_page('venues')) {
                        $newUrl = get_permalink( get_page_by_path('venues/new'));
                    } elseif(is_page('graduates')) {
                        $newUrl = get_permalink( get_page_by_path('graduates/new'));
                    } elseif(is_page('users')) {
                        $newUrl = get_permalink( get_page_by_path('users/new'));
                    }
                ?>
                <a href="<?php echo $newUrl; ?>" class="btn btn-primary">Nuevo +</a>
            <?php endif; ?>
        </div>
    </div>
</div>