<?php 
    // Get current page slug
    global $post;
    $post_slug = $post->post_name;

    // Get parent page Id
    $parentPageId = $post->post_parent;
    $parentPage = '';
    
    // If page has parent
    if($parentPageId) {
        // Get parent slug
        $parentPage = get_post_field( 'post_name', $parentPageId );
    }

    switch($post_slug) {
        case 'events' :
            get_template_part('partials/events/events-table');
            break;
        case 'venues' :
            get_template_part('partials/venues/venues-table');
            break;
        case 'graduates' :
            $graduates = get_users(array(
                'role' => 'graduate'
            ));
            include( locate_template( 'partials/graduates/graduates-table.php', false, false ) );
            break;
        case 'payments' :
            get_template_part('partials/payments/payments-table');
            break;
        case 'users' :
            get_template_part('partials/agents/agents-table');
            break;
        case 'settings' :
            gravity_form(8, false, false, false, false);
            break;
        case 'new' :
            switch($parentPage) {
                case 'events' :
                    gravity_form(2, false, false, false, false);
                    break;
                case 'venues' :
                    gravity_form(3, false, false, false, false);
                    break;
                case 'graduates' :
                    gravity_form(5, false, false, false, false);
                    break;
                case 'payments' :
                    get_template_part('partials/payments/payment-detail');
                    gravity_form(7, false, false, false, false);
                    break;
                case 'users' :
                    gravity_form(8, false, false, false, false);
                    break;
                default :
                    break;
            }
            break;
        case 'edit' :
            switch($parentPage) {
                case 'events' :
                    gravity_form(2, false, false, false, false);
                    break;
                case 'venues' :
                    gravity_form(3, false, false, false, false);
                    break;
                case 'graduates' :
                    gravity_form(6, false, false, false, false);
                    break;
                case 'users' :
                    gravity_form(8, false, false, false, false);
                    break;
                default :
                    break;
            }
            break;
        default: 
            break;
    }

    if(is_singular('event')) {
        $eventCode = get_field('codigo_del_evento');
        $graduates = get_users(array(
            'role' => 'graduate',
            'meta_key' => 'event_code',
            'meta_value' => $eventCode
        ));
        include( locate_template( 'partials/events/single.php', false, false ) );
        include( locate_template( 'partials/graduates/graduates-table.php', false, false ) );
    }
?>