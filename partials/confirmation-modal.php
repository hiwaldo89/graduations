<div class="modal fade" id="confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content py-4">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <h2 class="h3 text-center mb-4">
                        ¿Estás seguro que deseas eliminar el elemento?
                    </h2>
                    <?php if(is_page('events')) : ?>
                        <p class="text-center h6">
                            Se eliminarán todos los graduados y pagos asociados a este evento
                        </p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="modal-footer">
                <a id="delete-btn" href="" role="button" class="btn btn-danger mx-auto">Eliminar</a>
            </div>
        </div>
    </div>
</div>