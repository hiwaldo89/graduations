<div class="topbar d-flex">
    <div class="topbar-left">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">LOGO</a>
    </div>
    <nav class="navbar-custom d-flex align-items-center">
        <ul class="text-right list-inline m-0 p-0 ml-auto">
            <li><a href="<?php echo wp_logout_url( home_url() ); ?>">Cerrar sesión</a></li>
        </ul>
    </nav>
</div>