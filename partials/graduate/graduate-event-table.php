<div class="row">
    <div class="col-12">
        <?php
            $eventCode = get_user_meta($current_user->ID, 'event_code', true);
            $event = new WP_Query(array(
                'post_type'		=> 'event',
	            'meta_key'		=> 'codigo_del_evento',
	            'meta_value'	=> $eventCode
            ));
        ?>
        <?php if($event->have_posts()) : while($event->have_posts()) : $event->the_post(); ?>
            <h2 class="h4"><?php the_title(); ?></h2>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive mb-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>No. de mesa</th>
                                    <th>No. boletos</th>
                                    <th>Precio x boleto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>
                                        <?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?>
                                    </th>
                                    <th>
                                        N/A
                                    </th>
                                    <th>
                                        N/A
                                    </th>
                                    <th>
                                        $<?php the_field('precio_del_boleto'); ?>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</div>