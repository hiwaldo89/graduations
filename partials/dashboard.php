<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <?php get_template_part('partials/title-box'); ?>
            <?php get_template_part('partials/dashboard-content'); ?>
        </div>
    </div>
</div>