<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mb-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th class="text-right">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $venues = new WP_Query(array(
                                    'post_type' => 'venue',
                                    'posts_per_page' => 20
                                ));
                            ?>
                            <?php if($venues->have_posts()) : while($venues->have_posts()) : $venues->the_post(); ?>
                                <tr>
                                    <th><?php the_title(); ?></th>
                                    <th class="text-right">
                                        <a href="<?php echo get_permalink( get_page_by_path('venues/edit' )) . '?venue_id=' . get_the_ID(); ?>"><i class="fal fa-edit"></i></a>
                                        <a href="#" role="button" data-toggle="modal" data-target="#confirmation-modal" data-delete-link="<?php echo get_delete_post_link(get_the_ID(), '', true); ?>"><i class="fal fa-trash-alt"></i></a>
                                    </th>
                                </tr>
                            <?php endwhile; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>