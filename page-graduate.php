<?php
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }
    if( !is_user_logged_in() || !current_user_can('graduate') ) {
        //wp_redirect( site_url('/login') );
        exit;
    } 
?>

<?php
/**
 * Template Name: Graduate
 */

get_header(); ?>
    <?php $current_user = wp_get_current_user(); ?>
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <h1 class="h4 page-title"><?php echo '¡Hola ' . $current_user->user_firstname . '!'; ?></h1>
                        </div>
                    </div>
                </div>
                <?php include( locate_template( 'partials/graduate/graduate-event-table.php', false, false ) ); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>