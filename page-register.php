<?php
/**
 * Template Name: Register
 */

get_header(); ?>
    <div class="p-register">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 mx-auto">
                    <?php gravity_form( 4, false, false, false, false ); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>