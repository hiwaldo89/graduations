<?php
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }

    // If user is not logged in
    if( !is_user_logged_in() ) {
        //wp_redirect( site_url('/login') );
        exit;
    } 

    // If user is not admin
    if( current_user_can('graduate') ) {
        //wp_redirect( site_url('/graduado') );
        exit;
    }
    // if( !current_user_can('app_admin') ) {
    //     wp_redirect( site_url('/graduado') );
    //     exit;
    // }

?>

<?php
/**
 * Template Name: Dashboard
 */

get_header(); ?>
    <?php //get_template_part('partials/dashboard'); ?>
<?php get_footer(); ?>