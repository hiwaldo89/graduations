"use strict";

(function ($) {
  // Deletion confirmation modal
  $('#confirmation-modal').on('show.bs.modal', function (e) {
    var deleteUrl = $(e.relatedTarget).data('delete-link');
    var userId = $(e.relatedTarget).data('user-id');

    if (deleteUrl) {
      $('#delete-btn').attr('href', deleteUrl);
    } else if (userId) {
      $('#delete-btn').attr('href', userId);
    }
  }); // Delete users

  $('#delete-btn').on('click', function (e) {
    if (site.is_users_page) {
      e.preventDefault();
      var userId = $(this).attr('href');
      var data = {
        'action': 'delete_user_action',
        'user_id': userId,
        'security': site.ajax_nonce
      };
      $.post(site.ajax_url, data, function (response) {
        $('#confirmation-modal').modal('hide');
        $('#agent-' + userId).remove();
      });
    }
  }); // Init summernote

  $('.summernote').each(function () {
    $(this).find('textarea').summernote();
  });
})(jQuery);