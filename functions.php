<?php
    
    function graduations_setup() {
        // Make theme available for translation.
        load_theme_textdomain( 'graduations', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        // Let WordPress manage the document title.
        add_theme_support( 'title-tag' );

        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'menu-1' => esc_html__( 'Admin', 'graduations' ),
            'menu-2' => esc_html__( 'Sales Agent', 'graduations')
        ) );

        // Switch default core markup for search form, comment form, and comments to output valid HTML5.
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
    }
    add_action( 'after_setup_theme', 'graduations_setup' );

    // Enqueue scripts and styles.
    function jbm_scripts() {
        wp_enqueue_style( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,700' );
        wp_enqueue_style( 'graduations-style', get_stylesheet_uri() );

        wp_enqueue_script( 'fontawesome', 'https://pro.fontawesome.com/releases/v5.7.2/js/all.js', '', '', true );
        wp_enqueue_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '', true );
        wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );
        wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/assets/js/vendor.min.js', array('jquery'), '', true );
        wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.min.js', array('jquery'), '', true );

        wp_localize_script('custom-js', 'site', array(
            'is_users_page' => is_page('users'),
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'ajax_nonce' => wp_create_nonce( "security-special-string" )
        ));
    }
    add_action( 'wp_enqueue_scripts', 'jbm_scripts' );

    // Hook the appropriate WordPress action
    // add_action('init', 'graduations_prevent_wp_login');
    // function graduations_prevent_wp_login() {
    //     // WP tracks the current page - global the variable to access it
    //     global $pagenow;
    //     // Check if a $_GET['action'] is set, and if so, load it into $action variable
    //     $action = (isset($_GET['action'])) ? $_GET['action'] : '';
    //     // Check if we're on the login page, and ensure the action is not 'logout'
    //     if( $pagenow == 'wp-login.php' && ( ! $action || ( $action && ! in_array($action, array('logout', 'lostpassword', 'rp', 'resetpass'))))) {
    //         // Load the home page url
    //         $page = get_bloginfo('url');
    //         // Redirect to the home page
    //         wp_redirect($page);
    //         // Stop execution to prevent the page loading for any reason
    //         exit();
    //     }
    // }

    // User roles
    require_once get_template_directory() . '/inc/graduations-users.php';

    // GravityForms compatibility file
    require_once get_template_directory() . '/inc/graduations-gravityforms.php';

    // Rest API
    require_once get_template_directory() . '/inc/graduations-api.php';

    // Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );

add_action('rest_api_init', 'wp_rest_user_endpoints');


/**
 * Register a new user
 *
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/
function wp_rest_user_endpoints($request) {
  /**
   * Handle Register User request.
   */
  register_rest_route('wp/v2', 'users/register', array(
    'methods' => 'POST',
    'callback' => 'wc_rest_user_endpoint_handler',
  ));
}
function wc_rest_user_endpoint_handler($request = null) {
  $response = array();
  $parameters = $request->get_json_params();
  $username = sanitize_text_field($parameters['username']);
  $email = sanitize_text_field($parameters['email']);
  $password = sanitize_text_field($parameters['password']);
  $first_name = sanitize_text_field($parameters['first_name']);
  $last_name = sanitize_text_field($parameters['last_name']);
  $event = sanitize_text_field($parameters['event']);
  $phone = sanitize_text_field($parameters['phone']);
  $tickets = sanitize_text_field($parameters['tickets']);
  $special_menu = $parameters['special_menu'];
  // $role = sanitize_text_field($parameters['role']);
  $error = new WP_Error();
  if (empty($username)) {
    $error->add(400, __("Username field 'username' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  if (empty($email)) {
    $error->add(401, __("Email field 'email' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  if (empty($password)) {
    $error->add(404, __("Password field 'password' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  // if (empty($role)) {
  //  $role = 'subscriber';
  // } else {
  //     if ($GLOBALS['wp_roles']->is_role($role)) {
  //      // Silence is gold
  //     } else {
  //    $error->add(405, __("Role field 'role' is not a valid. Check your User Roles from Dashboard.", 'wp_rest_user'), array('status' => 400));
  //    return $error;
  //     }
  // }
  $user_id = username_exists($username);
  if (!$user_id && email_exists($email) == false) {
    $user_id = wp_create_user($username, $password, $email);
    if (!is_wp_error($user_id)) {
      // Ger User Meta Data (Sensitive, Password included. DO NOT pass to front end.)
      $user = get_user_by('id', $user_id);
      // $user->set_role($role);
      $user->set_role('graduate');
        wp_update_user(array(
            'ID' => $user_id,
            'first_name' => $first_name,
            'last_name' => $last_name
        ));
      // Update user acf
      //update_field( 'field_5c99a845b1091', $event_code, 'user_' . $user_id );
      update_field( 'tickets', $tickets, 'user_' . $user_id );
      update_field( 'field_5d0066d90fe3b', $event, 'user_' . $user_id );
      update_field( 'field_5c9ab3f588482', $phone, 'user_' . $user_id );

      $specialMenus = array(
        'silla_de_ruedas' => $special_menu['wheelchair'],
        'ninos' => $special_menu['kids'],
        'alergicos' => $special_menu['allergies'],
        'diabeticos' => $special_menu['diabetic'],
        'vegetarianos' => $special_menu['vegetarian']
      );

      update_field( 'menu_especial', $specialMenus, 'user_' . $user_id );

      $eventId = get_field( 'event', 'user_' . $user_id );
      $ticket_price = get_field( 'precio_del_boleto', $eventId );
      $discount = $special_menu['kids'] * 100;
      $total = ($ticket_price * $tickets) - $discount;
      $oldTotal = get_field( 'total', 'user_' . $user_id );
      update_field( 'total', $total, 'user_' . $user_id );
      
      // $eventBalance = get_field( 'balance', $eventId );
      // $eventBalanceChange = $total - $oldTotal;
      // $newBalance = $eventBalance + $eventBalanceChange;
      // update_field( 'balance', $newBalance, $eventId );

      
      $code = sha1( $user_id . time() );
      update_field( 'verification_code', $code, 'user_' . $user_id );
      $activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_field('app_url', 'option') . '/verify');
      wp_mail($email, 'Verifica tu cuenta', 'Gracias por registrarte. Puedes verificar tu cuenta dando click en el siguiente link: ' . $activation_link);

      $response['code'] = 200;
      $response['message'] = __("User '" . $username . "' Registration was Successful", "wp-rest-user");
    } else {
      return $user_id;
    }
  } else {
    $error->add(406, __("Email already exists, please try 'Reset Password'", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  return new WP_REST_Response($response, 123);
}

function get_user_roles($object, $field_name, $request) {
  return get_userdata($object['id'])->roles;
}

add_action('rest_api_init', function() {
  register_rest_field('user', 'roles', array(
    'get_callback' => 'get_user_roles',
    'update_callback' => null,
    'schema' => array(
      'type' => 'array'
    )
  ));
});

// Add Advanced Custom Fields options page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Site Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'site-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}