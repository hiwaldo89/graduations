<?php 
    // Populate form with graduate email
    add_filter( 'gform_field_value_payment_user_email', 'graduations_populate_graduate_email' );
    function graduations_populate_graduate_email($value) {
        $graduate = get_user_by('id', $_GET['user_id']);
        return $graduate->user_email;
    }

    // Create payments
    add_action( 'gform_after_submission_7', 'graduations_create_payment', 10, 2 );
    function graduations_create_payment($entry, $form) {
        $graduate = get_user_by('id', rgar($entry, 3));
        $post_id = wp_insert_post(array(
            'post_title' => $graduate->user_firstname . ' ' . $graduate->user_lastname,
            'post_status' => 'publish',
            'post_type' => 'payment'
        ), true);

        // Quantity input
        update_field( 'field_5c9bf2f5023f2', rgar($entry, 1), $post_id );

        // User Id input
        update_field( 'field_5c9bf3be9d835', rgar($entry, 3), $post_id );

        // Event code input
        update_field( 'field_5c9bfa2e18c92', rgar($entry, 4), $post_id );

        // update user remaining
        $user_id = (int) rgar($entry, 3);
        $currentRemaining = (int) get_user_meta(rgar($entry, 3), 'remaining', true);
        $amountPaid = (int) rgar($entry, 1);
        $newRemaining = $currentRemaining - $amountPaid;
        update_field( 'field_5c9c1da4a5cc7', $newRemaining, 'user_' . $user_id);

        // update event balance
        $eventCode = rgar($entry, 4);
        $graduates = get_users(array(
            'role' => 'graduate',
            'meta_key' => 'event_code',
            'meta_value' => $eventCode
        ));
        $event = get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'event',
            'meta_key'		=> 'codigo_del_evento',
            'meta_value'	=> $eventCode
        ));
        $eventRemaining = 0;
        foreach($graduates as $graduate) {
            $eventRemaining += get_user_meta($graduate->ID, 'remaining', true);
        }
        update_field( 'field_5c9c27aa997f0', $eventRemaining, $event[0]->ID);

        // Delete entry
        GFAPI::delete_entry( $entry['id'] );
    }