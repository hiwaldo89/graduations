<?php
    // Populate edit event with correct info
    add_filter( 'gform_field_value', 'venues_populate_event', 10, 3 );
    function venues_populate_event( $value, $field, $name ) {
        if(is_page('venues/edit')) {
            $venue = get_post($_GET['venue_id']);
            $values = array(
                'venue_title'   => $venue->post_title
            );
            return isset( $values[ $name ] ) ? $values[ $name ] : $value;
        } else {
            return $value;
        }
    }

    // Create Venue
    add_action( 'gform_after_submission_3', 'graduations_create_venue', 10, 2 );
    function graduations_create_venue($entry, $form) {
        if(is_page('venues/new')) {
            $post_id = wp_insert_post(array(
                'post_title' => rgar( $entry, 1 ),
                'post_status' => 'publish',
                'post_type' => 'venue'
            ), true);
        } elseif(is_page('venues/edit')) {
            $post_id = $_GET['venue_id'];
            wp_update_post(array(
                'ID' => $post_id,
                'post_title' => rgar($entry, 1)
            ));
        }

        // Upload images to media library
        if( rgar($entry, 2) !== '') {
            $files = rgar($entry, 2);
            $attachment_ids = array();
        }
        foreach($form['fields'] as $field) {
            if( $field['type'] == 'fileupload' ) {
                $entry_id = $field['id'];
                $files = rgar ( $entry, $entry_id );
                $patterns = ['[', ']', '"'];
                $file_entry = str_replace($patterns, '', $files);
                $files = explode ( ',', $file_entry  );
                $attachment_ids = array();
                foreach ($files as $file) {
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    include_once( ABSPATH . 'wp-admin/includes/image.php' );
                    include_once( ABSPATH . 'wp-admin/includes/file.php' );
                    include_once( ABSPATH . 'wp-admin/includes/media.php' );
                    $new_url = stripslashes($file);
                    $result = media_sideload_image( $new_url, $post_id, $filename, 'src');
                    $attachment_ids[] = get_attachment_id_from_src($result);
                }
                update_field( 'field_5c97d9fa8bfdc', $attachment_ids, $post_id );
            }
        } 
        // Delete entry
        GFAPI::delete_entry( $entry['id'] );
    }