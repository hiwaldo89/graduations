<?php

if ( class_exists( 'GFCommon' ) ) {

    // filter the Gravity Forms button type
    add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
    function form_submit_button( $button, $form ) {
        // return "<button class='btn btn-primary animated' id='gform_submit_button_{$form['id']}'>" . $form['button']['text'] . "</button>";
        $dom = new DOMDocument();
        $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $button );
        $input = $dom->getElementsByTagName( 'input' )->item(0);
        $new_button = $dom->createElement( 'button' );
        $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
        $input->removeAttribute( 'value' );
        foreach( $input->attributes as $attribute ) {
            $new_button->setAttribute( $attribute->name, $attribute->value );
        }
        $input->parentNode->replaceChild( $new_button, $input );

        $new_button->setAttribute( 'class', "btn btn-primary" );

        return $dom->saveHtml( $new_button );
    }

    // Get attachment ID from source
    function get_attachment_id_from_src($image_src) {
        global $wpdb;
        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
        $id = $wpdb->get_var($query);
        return $id;
    }

    // Login
    require_once get_template_directory() . '/inc/graduations-login.php';

    // Events
    require_once get_template_directory() . '/inc/graduations-events.php';

    // Venues
    require_once get_template_directory() . '/inc/graduations-venues.php';

    // Graduates
    require_once get_template_directory() . '/inc/graduations-graduates.php';

    // Payments
    require_once get_template_directory() . '/inc/graduations-payments.php';

    // Sales agents
    require_once get_template_directory() . '/inc/graduations-agents.php';
}