<?php
    // Populate edit event with correct info
    add_filter( 'gform_field_value', 'graduations_populate_event', 10, 3 );
    function graduations_populate_event( $value, $field, $name ) {
        if(is_page('events/edit')) {
            $event = get_post($_GET['event_id']);
            $values = array(
                'event_title'   => $event->post_title,
                'event_school' => get_field('escuela', $event->ID),
                'event_date' => get_field('fecha', $event->ID),
                'event_ticket_price' => get_field('precio_del_boleto', $event->ID),
                'event_code' => get_field('codigo_del_evento', $event->ID),
                'event_venue' => get_field('venue', $event->ID)->ID,
                'event_description' => get_field('description', $event->ID),
                'event_fb_url' => get_field('facebook_url', $event->ID)
            );
            return isset( $values[ $name ] ) ? $values[ $name ] : $value;
        } else {
            return $value;
        }
    }

    // Create Events
    add_action( 'gform_after_submission_2', 'graduations_create_event', 10, 2 );
    function graduations_create_event($entry, $form) {
        if(is_page('events/new')) {
            $post_id = wp_insert_post(array(
                'post_title' => rgar( $entry, 1 ),
                'post_status' => 'publish',
                'post_type' => 'event'
            ), true);
        } elseif(is_page('events/edit')) {
            $post_id = $_GET['event_id'];
            wp_update_post(array(
                'ID' => $post_id,
                'post_title' => rgar($entry, 1)
            ));
        }

        // School input
        update_field( 'field_5c968f3542b4f', rgar($entry, 2), $post_id );

        // Date input
        update_field( 'field_5c968f4742b50', rgar($entry, 3), $post_id );

        // Price input
        update_field( 'field_5c995bf1f94a8', rgar($entry, 11), $post_id );

        // Event code input
        update_field( 'field_5c968fd542b51', rgar($entry, 4), $post_id );

        // Venue input
        update_field( 'field_5c968ff442b52', rgar($entry, 10), $post_id );

        // Description input
        update_field( 'field_5c9e9e01a5c3a', rgar($entry, 12), $post_id );

        // Facebook url input
        update_field( 'field_5ca18a9e7de00', rgar($entry, 13), $post_id) ;

        // Layout input
        if( rgar($entry, 7) !== '') {
            if(get_field('layout', $post_id)) {
                $currentFile = get_field('layout', $post_id);
                wp_delete_attachment($currentFile['ID'], true);
            } 
            $file = rgar($entry, 7);
            $filename = pathinfo($file, PATHINFO_FILENAME);
            include_once( ABSPATH . 'wp-admin/includes/image.php' );
            include_once( ABSPATH . 'wp-admin/includes/file.php' );
            include_once( ABSPATH . 'wp-admin/includes/media.php' );
            $new_url = stripslashes($file);
            $result = media_sideload_image( $new_url, $post_id, $filename, 'src');
            $attachment_id = (int)  get_attachment_id_from_src($result);
            update_field( 'field_5c96900042b53', $attachment_id, $post_id );
        }
        
        // Delete entry
        GFAPI::delete_entry( $entry['id'] );
    }

    

    // Event code generation
    add_filter( 'gform_field_value_event_code', 'graduations_event_code' );
    function graduations_event_code($value) {
        if(is_page('events/new')) {
            $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
            srand((double)microtime()*1000000); 
            $i = 0; 
            $pass = '' ; 

            while ($i <= 7) { 
                $num = rand() % 33; 
                $tmp = substr($chars, $num, 1); 
                $pass = $pass . $tmp; 
                $i++; 
            } 

            return $pass; 
        } else {
            return $value;
        }
    }

    // Populate venues
    add_filter("gform_pre_render_2", 'graduations_populate_venues');
    function graduations_populate_venues($form) {
        $venues = get_posts(array(
            'post_type' => 'venue',
            'posts_per_page' => -1
        ));
        //Creating drop down item array.
        $items = array();

        //Adding initial blank value.
        $items[] = array("text" => "", "value" => "");

        foreach($venues as $venue) {
            $items[] = array("value" => $venue->ID, "text" => $venue->post_title);
        }

        foreach($form["fields"] as $field) {
            if($field["id"] == 10){
                $field["type"] = "select";
                $field["choices"] = $items;
            }
        }
        return $form;
    }

    // Event deletion
    add_action( 'before_delete_post', 'graduations_delete_event', 10, 1 ); 
    function graduations_delete_event($postId) {
        global $post_type;
        if($post_type != 'event') {
            return;
        } else {
            $eventCode = get_field('codigo_del_evento', $postId);
            $payments = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'payment',
                'meta_query' => array(
                    array(
                        'key' => 'event_code',
                        'value' => $eventCode
                    )
                )
            ));
            $graduates = get_users(array(
                'role' => 'graduate',
                'meta_key' => 'event_code',
                'meta_value' => $eventCode
            ));
            // Delete everything associated with the event
            foreach($payments as $payment) {
                wp_delete_post($payment->ID, true);
            }
            foreach($graduates as $graduate) {
                wp_delete_user($graduate->ID);
            }
        }
    }