<?php

    // App admin role
    add_role('app_admin', __('App Administrator'), array(
        'publish_posts' => true,
        'read' => true,
        'delete_posts' => true,
        'delete_private_posts' => true,
        'delete_published_posts' => true,
        'delete_others_posts' => true,
        'create_users' => true,
        'edit_users' => true,
        'delete_users' => true,
        'list_users' => true,
        'remove_users' => true,
        'promote_users' => true,
        'add_users' => true,
        'edit_others_posts' => true,
        'edit_posts' => true,
        'edit_private_posts' => true,
        'edit_published_posts' => true,
        'upload_files' => true
    ));

    //remove_role('sales_agent');

    // Sales agent role
    add_role('sales_agent', __('Sales Agent'), array(
        'publish_posts' => true, 
        'read' => true,
        'list_users' => true,
        'edit_users' => true,
        'edit_posts' => true,
        'edit_private_posts' => true,
        'edit_published_posts' => true,
        'upload_files' => true
    ));

    // Graduate role
    add_role('graduate', __('Graduate'), array(

    ));

    // Remove toolbar
    add_action('after_setup_theme', 'graduations_remove_admin_bar');
    function graduations_remove_admin_bar() {
        if (!current_user_can('administrator') && !is_admin()) {
            show_admin_bar(false);
        }
    }