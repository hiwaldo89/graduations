<?php
    // event.json
    function graduations_rest_prepare_event($data, $post, $request) {
        $_data = $data->data;
        
        // Add ACF to API
        $fields = get_fields($post->ID);
        if($fields) {
          foreach ($fields as $key => $value){
            $_data[$key] = get_field($key, $post->ID);
          }
        }
        $data->data = $_data;
        
        return $data;
    }
    add_filter("rest_prepare_event", 'graduations_rest_prepare_event', 10, 3);

    // venue.json
    function graduations_rest_prepare_venue($data, $post, $request) {
      $_data = $data->data;
      
      // Add ACF to API
      $fields = get_fields($post->ID);
      foreach ($fields as $key => $value){
        $_data[$key] = get_field($key, $post->ID);
      }
      $data->data = $_data;
      
      return $data;
    }
    add_filter("rest_prepare_venue", 'graduations_rest_prepare_venue', 10, 3);

    // users.json
    function graduations_add_acf_to_user() {
      register_rest_field('user', 'data', array(
        'get_callback' => 'testing',
        'update_callback'   => null,
        'schema'            => null,
      ));
    }
    add_action( 'rest_api_init', 'graduations_add_acf_to_user' );
    function testing($user, $field_name, $request) {
      $fields = get_fields('user_' . $user['id']);
      $_data = [];
      $_data['name'] = $user['first_name'];
      $_data['lastname'] = $user['last_name'];
      $_data['email'] = $user['email'];
      if($fields) {
        foreach ($fields as $key => $value){
          $_data[$key] = get_field($key, 'user_' . $user['id']);
        }
      }
      //return get_user_meta($user['id'], $field_name, true);
      return $_data;
    }