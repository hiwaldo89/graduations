<?php
// event.json
function graduations_rest_prepare_event($data, $post, $request) {
    $_data = $data->data;
    
    // Add ACF to API
    $fields = get_fields($post->ID);
    if($fields) {
      foreach ($fields as $key => $value){
        $_data[$key] = get_field($key, $post->ID);
      }
    }
    $data->data = $_data;
    
    return $data;
}
add_filter("rest_prepare_event", 'graduations_rest_prepare_event', 10, 3);

// venue.json
function graduations_rest_prepare_venue($data, $post, $request) {
    $_data = $data->data;
    
    // Add ACF to API
    $fields = get_fields($post->ID);
    foreach ($fields as $key => $value){
      $_data[$key] = get_field($key, $post->ID);
    }
    $data->data = $_data;
    
    return $data;
}
add_filter("rest_prepare_venue", 'graduations_rest_prepare_venue', 10, 3);

// users.json
function graduations_rest_prepare_users($response, $user, $request) {
    $_data = $response->data;
    // Add ACF to API
    $fields = get_fields('user_' . $user->ID);
    if($fields) {
        foreach ($fields as $key => $value){
            $_data[$key] = get_field($key, 'user_' . $user->ID);
        }
    }   
    $_data['firstname'] = $user->first_name;
    $_data['lastname'] = $user->last_name;
    $_data['email'] = $user->user_email;
    $response->data = $_data;
    
    return $response;
}
add_filter("rest_prepare_user", 'graduations_rest_prepare_users', 10, 3);

// payments.json
function graduations_rest_prepare_payments($data, $post, $request) {
    $_data = $data->data;
    
    // Add ACF to API
    $fields = get_fields($post->ID);
    foreach ($fields as $key => $value){
      $_data[$key] = get_field($key, $post->ID);
    }
    $data->data = $_data;
    
    return $data;
}
add_filter("rest_prepare_payment", 'graduations_rest_prepare_payments', 10, 3);

add_action('rest_api_init', 'graduations_rest_endpoints');
function graduations_rest_endpoints($request) {
    register_rest_route('graduations/v1', 'events', array(
        'methods' => 'POST',
        'callback' => 'graduations_rest_events_endpoint_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin') || current_user_can('sales_agent'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'events/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'graduations_rest_events_patch_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin') || current_user_can('sales_agent'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'venues', array(
        'methods' => 'POST',
        'callback' => 'graduations_rest_venues_endpoint_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'venues/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'graduations_rest_venues_patch_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'graduates/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'graduations_rest_graduates_patch_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin') || current_user_can('sales_agent'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'payments', array(
        'methods' => 'POST',
        'callback' => 'graduations_rest_payments_endpoint_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin') || current_user_can('sales_agent'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'agents', array(
        'methods' => 'POST',
        'callback' => 'graduations_rest_agents_endpoint_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'agents/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'graduations_rest_agents_patch_handler',
        'permission_callback' => function ($request) {
            if (current_user_can('app_admin'))
            return true;
        }
    ));

    register_rest_route('graduations/v1', 'verify-user/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'graduations_rest_verify_graduate_handler',
    ));

    register_rest_route('graduations/v1', 'send-confirmation/(?P<id>\d+)', array(
        'methods' => 'POST',
        'callback' => 'graduations_rest_resend_confirmation_email',
        'permission_callback' => function ($request) {
            if(current_user_can('app_admin') || current_user_can('sales_agent'))
            return true;
        }
    ));
}

// POST graduations/v1/events
function graduations_rest_events_endpoint_handler($request = null) {
    $response = array();
    $parameters = $request->get_json_params();
    $title = sanitize_text_field($parameters['title']);
    $school = sanitize_text_field($parameters['school']);
    $date = sanitize_text_field($parameters['date']);
    $ticket_price = sanitize_text_field($parameters['ticket_price']);
    $venue = sanitize_text_field($parameters['venue']);
    $layout = sanitize_text_field($parameters['layout']);
    $description = $parameters['description'];
    $facebook_url = sanitize_text_field($parameters['facebook_url']);

    $error = new WP_Error();
    $post_id = wp_insert_post(array(
        'post_title' => $title,
        'post_status' => 'publish',
        'post_type' => 'event'
    ), true);
    if (!is_wp_error($post_id)) {
        update_field( 'escuela', $school, $post_id );
        update_field( 'fecha', $date, $post_id );
        update_field( 'precio_del_boleto', $ticket_price, $post_id );
        update_field( 'venue', $venue, $post_id );
        update_field( 'layout', $layout, $post_id );
        update_field( 'description', $description, $post_id );
        update_field( 'facebook_url', $facebook_url, $post_id );
        $response['code'] = 200;
    }
    return new WP_REST_Response($post_id);
}

// PATCH graduations/v1/events/id
function graduations_rest_events_patch_handler($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $post_id = $request['id'];
    $title = sanitize_text_field($parameters['title']);
    $school = sanitize_text_field($parameters['school']);
    $date = sanitize_text_field($parameters['date']);
    $ticket_price = sanitize_text_field($parameters['ticket_price']);
    $venue = sanitize_text_field($parameters['venue']);
    $layout = sanitize_text_field($parameters['layout']);
    $description = $parameters['description'];
    $facebook_url = sanitize_text_field($parameters['facebook_url']);

    wp_update_post(array(
        'ID' => $post_id,
        'post_title' => $title
    ));
    update_field( 'escuela', $school, $post_id );
    update_field( 'fecha', $date, $post_id );
    update_field( 'precio_del_boleto', $ticket_price, $post_id );
    update_field( 'venue', $venue, $post_id );
    update_field( 'layout', $layout, $post_id );
    update_field( 'description', $description, $post_id );
    update_field( 'facebook_url', $facebook_url, $post_id );
    $response['code'] = 200;
    return new WP_REST_Response($response, 123);
}

// POST graduations/v1/venues
function graduations_rest_venues_endpoint_handler($request = null) {
    $response = array();
    $parameters = $request->get_json_params();
    $title = sanitize_text_field($parameters['title']);
    $gallery = $parameters['gallery'];
    $description = $parameters['description'];

    $error = new WP_Error();

    $post_id = wp_insert_post(array(
        'post_title' => $title,
        'post_status' => 'publish',
        'post_type' => 'venue'
    ), true);
    if (!is_wp_error($post_id)) {
        update_field( 'gallery', $gallery, $post_id );
        update_field( 'description', $description, $post_id );
        $response['code'] = 200;
    }
    return new WP_REST_Response($response, 123);
}

// PATCH graduations/v1/venues/id
function graduations_rest_venues_patch_handler($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $post_id = $request['id'];
    $title = sanitize_text_field($parameters['title']);
    $gallery = $parameters['gallery'];
    $events = $parameters['events'];
    $description = $parameters['description'];

    wp_update_post(array(
        'ID' => $post_id,
        'post_title' => $title
    ));
    update_field( 'gallery', $gallery, $post_id );
    update_field( 'events', $events, $post_id );
    update_field( 'description', $description, $post_id );
    $response['code'] = 200;
    return new WP_REST_Response($response, 123);
}

// PATCH graduations/v1/graduates/id
function graduations_rest_graduates_patch_handler($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $user_id = $request['id'];
    $username = sanitize_text_field($parameters['username']);
    $email = sanitize_text_field($parameters['email']);
    $first_name = sanitize_text_field($parameters['first_name']);
    $last_name = sanitize_text_field($parameters['last_name']);
    $tickets = sanitize_text_field($parameters['tickets']);
    $table = sanitize_text_field($parameters['table']);
    $special_menu = $parameters['special_menu'];

    wp_update_user(array(
        'ID' => $user_id,
        'user_login' => $username,
        'user_email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name
    ));
    update_field( 'tickets', $tickets, 'user_' . $user_id );
    update_field( 'table_number', $table, 'user_' . $user_id );

    $specialMenus = array(
        'silla_de_ruedas' => $special_menu['wheelchair'],
        'ninos' => $special_menu['kids'],
        'alergicos' => $special_menu['allergies'],
        'diabeticos' => $special_menu['diabetic'],
        'vegetarianos' => $special_menu['vegetarian']
    );

    update_field( 'menu_especial', $specialMenus, 'user_' . $user_id );

    $eventId = get_field( 'event', 'user_' . $user_id );
    $ticket_price = get_field( 'precio_del_boleto', $eventId );
    $discount = $special_menu['kids'] * 100;
    $total = ($ticket_price * $tickets) - $discount;
    $oldTotal = get_field( 'total', 'user_' . $user_id );
    update_field( 'total', $total, 'user_' . $user_id );
    
    $eventBalance = get_field( 'balance', $eventId );
    $eventBalanceChange = $total - $oldTotal;
    $newBalance = $eventBalance + $eventBalanceChange;
    update_field( 'balance', $newBalance, $eventId ); 

    $response['code'] = 200;
    return new WP_REST_Response($response, 123);
}

// POST graduations/v1/payments
function graduations_rest_payments_endpoint_handler($request = null) {
    $response = array();
    $parameters = $request->get_json_params();
    $user_name = sanitize_text_field($parameters['graduateName']);
    $quantity = sanitize_text_field($parameters['quantity']);
    $user_id = sanitize_text_field($parameters['graduateId']);
    $event_id = sanitize_text_field($parameters['eventId']);
    $agent = sanitize_text_field($parameters['agent']);

    $error = new WP_Error();

    $post_id = wp_insert_post(array(
        'post_title' => $user_name,
        'post_status' => 'publish',
        'post_type' => 'payment'
    ), true);
    if (!is_wp_error($post_id)) {
        update_field( 'quantity', $quantity, $post_id );
        update_field( 'user_id', $user_id, $post_id );
        update_field( 'event_id', $event_id, $post_id );
        update_field( 'agent', $agent, $post_id );

        $totalPaid = get_field('total_paid', 'user_' . $user_id );
        $totalPaid = $totalPaid + $quantity;
        update_field( 'total_paid', $totalPaid, 'user_' . $user_id );
        $total = get_field( 'total', 'user_' . $user_id );
        $remaining = $total - $totalPaid;
        update_field( 'saldo', $remaining, $post_id );

        $eventBalance = get_field( 'balance', $event_id );
        $newBalance = $eventBalance - $quantity;
        update_field( 'balance', $newBalance, $event_id );

        $response['code'] = 200;

        $graduate = get_userdata($user_id);
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $totalTickets = get_field('tickets', 'user_' . $user_id);
        if ((int)$quantity < 0) {
            $body = '<img src="https://promnite.com.mx/wp-content/themes/promnite/assets/img/logo.png" width="80"><br><h2>¡Hola ' . $user_name . '!</h2><p>Se te ha hecho un reembolso por la cantidad de $' . $quantity . '</p><p><strong>Número de boletos:</strong> ' . $totalTickets . '</p><p><strong>Total pagado:</strong> $' . $totalPaid . '</p><p><strong>Saldo restante:</strong> $' . $remaining . '</p><hr>Atentamene, equipo PromNite.';
            wp_mail($graduate->user_email, 'Se ha registrado un reembolso', $body, $headers);
        } else {
            $body = '<img src="https://promnite.com.mx/wp-content/themes/promnite/assets/img/logo.png" width="80"><br><h2>¡Hola ' . $user_name . '!</h2><p>Recibimos tu pago por la cantidad de $' . $quantity . '</p><p><strong>Número de boletos:</strong> ' . $totalTickets . '</p><p><strong>Total pagado:</strong> $' . $totalPaid . '</p><p><strong>Saldo restante:</strong> $' . $remaining . '</p><hr>Atentamente, equipo PromNite.';
            wp_mail($graduate->user_email, 'Gracias por tu pago', $body, $headers);
        }
    }
    return new WP_REST_Response($response, 123);
}

// POST graduations/v1/agents
function graduations_rest_agents_endpoint_handler($request = null) {
    $response = array();
    $parameters = $request->get_json_params();
    $username = sanitize_text_field($parameters['username']);
    $email = sanitize_text_field($parameters['email']);
    $first_name = sanitize_text_field($parameters['first_name']);
    $last_name = sanitize_text_field($parameters['last_name']);
    $phone = sanitize_text_field($parameters['phone']);
    $password = sanitize_text_field($parameters['password']);

    $user_id = username_exists($username);
    if (!$user_id && email_exists($email) == false) {
        $user_id = wp_create_user($username, $password, $email);
        if (!is_wp_error($user_id)) {
            $user = get_user_by('id', $user_id);
            $user->set_role('sales_agent');
            wp_update_user(array(
                'ID' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name
            ));
            update_field( 'phone', $phone, 'user_' . $user_id );
            wp_mail($email, 'Bienvenido', 'Se ha registrado una cuenta con tu correo en la aplicación de graduaciones.');
        }
    }
}

// PATCH graduations/v1/agents/id
function graduations_rest_agents_patch_handler($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $user_id = $request['id'];
    $username = sanitize_text_field($parameters['username']);
    $email = sanitize_text_field($parameters['email']);
    $first_name = sanitize_text_field($parameters['first_name']);
    $last_name = sanitize_text_field($parameters['last_name']);
    $phone = sanitize_text_field($parameters['phone']);

    wp_update_user(array(
        'ID' => $user_id,
        'user_login' => $username,
        'user_email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name
    ));
    update_field( 'phone', $phone, 'user_' . $user_id );
    $response['code'] = 200;
    return new WP_REST_Response($response, 123);
}

// PATCH graduations/v1/verify-user/id
function graduations_rest_verify_graduate_handler($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $user_id = $request['id'];
    $verification_code = sanitize_text_field($parameters['verification_code']);

    $error = new WP_Error();

    $userCode = get_field('verification_code', 'user_' . $user_id);
    if ($userCode === $verification_code && !get_field('verificado', 'user_' . $user_id)) {
        update_field('verificado', 1, 'user_' . $user_id);
        $eventId = get_field('event', 'user_' . $user_id);

        $total = get_field( 'total', 'user_' . $user_id );
      
        $eventBalance = get_field( 'balance', $eventId );
        $newBalance = $eventBalance + $total;
        update_field( 'balance', $newBalance, $eventId );
        $response['code'] = 200;
        return new WP_REST_Response($response, 123);
    } else {
        $error->add(400, __("Incorrect verification code", 'wp-rest-user'), array('status' => 400));
        return $error;
    }
}

// POST graduations/v1/send-confirmation/id
function graduations_rest_resend_confirmation_email($request) {
    $response = array();
    $parameters = $request->get_json_params();
    $user_id = $request['id'];

    $user_info = get_userdata($user_id);
    $email = $user_info->user_email;

    $code = sha1( $user_id . time() );
    update_field( 'verification_code', $code, 'user_' . $user_id );
    $activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_field('app_url', 'option') . '/verify');
    wp_mail($email, 'Verifica tu cuenta', 'Gracias por registrarte. Puedes verificar tu cuenta dando click en el siguiente link: ' . $activation_link);

    $response['code'] = 200;
    return new WP_REST_Response($response, 123);
}