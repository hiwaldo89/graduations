<?php
    // Handle login form
    add_action( 'gform_after_submission_1', 'graduations_login', 10, 2 );
    function graduations_login($entry, $form) {
        // get the username and pass
        $username = $entry[1];
        $password = $entry[2];

        $credentials = array();

        // create the credentials array
        $credentials['user_login'] = $username;
        $credentials['user_password'] = $password;

        // sign in the user and set the logged in user
        $sign = wp_signon( $credentials );
        wp_set_current_user( $sign->ID );

        // Delete entry
        GFAPI::delete_entry( $entry['id'] );

        if(current_user_can('app_admin') || current_user_can('sales_agent')) {
            wp_redirect( site_url() );
        } elseif(current_user_can('administrator')) {
            wp_redirect( site_url('/wp-admin' ));
        } else {
            wp_redirect( site_url('/graduate') );
        }
    }

    // Validate login form data
    add_filter( "gform_field_validation_1", "graduations_login_validation", 10, 4 );
    function graduations_login_validation($result, $value, $form, $field) {
        global $user;

        // validate username
        if ( strpos($field['cssClass'], 'username') !== false ) {
            $user = get_user_by( 'login', $value );
            if ( empty( $user->user_login ) ) {
                $result["is_valid"] = false;
                $result["message"] = "Invalid username provided.";
            }
        }

        // validate password
        if ( strpos($field['cssClass'], 'password') !== false ) {
            if ( !$user or !wp_check_password( $value, $user->data->user_pass, $user->ID ) ) {
                $result["is_valid"] = false;
                $result["message"] = "Invalid password provided.";
            }
        }
        return $result;
    }