<?php
    // Populate edit graduate with correct info
    add_filter( 'gform_field_value', 'graduations_populate_graduate', 10, 3 );
    function graduations_populate_graduate( $value, $field, $name ) {
        if(is_page('graduates/edit')) {
            $graduate = get_user_by('id', $_GET['user_id']);
            $specialMenus = get_field('menu_especial', 'user_' . $graduate->ID);
            $values = array(
                'graduates_username'   => $graduate->user_login,
                'graduates_email' => $graduate->user_email,
                'graduates_name' => $graduate->user_firstname,
                'graduates_lastname' => $graduate->user_lastname,
                'graduates_phone' => get_user_meta($graduate->ID, 'phone', true),
                'edit_user_event_code' => get_user_meta($graduate->ID, 'event_code', true),
                'graduates_tickets' => get_user_meta($graduate->ID, 'tickets', true),
                'graduates_table' => get_user_meta($graduate->ID, 'table_number', true),
                'graduates_special_menu_1' => $specialMenus['silla_de_ruedas'],
                'graduates_special_menu_2' => $specialMenus['ninos'],
                'graduates_special_menu_3' => $specialMenus['alergicos'],
                'graduates_special_menu_4' => $specialMenus['diabeticos'],
                'graduates_special_menu_5' => $specialMenus['vegetarianos']
            );
        
            return isset( $values[ $name ] ) ? $values[ $name ] : $value;
        } else {
            return $value;
        }
    }

    // Edit graduate on form submission
    add_action( 'gform_after_submission_6', 'graduations_update_graduate', 10, 2 );
    function graduations_update_graduate($entry, $form) {
        if(is_page('graduates/edit') && (current_user_can('app_admin') || current_user_can('sales_agent'))) {
            $user_id = (int) $_GET['user_id'];
            if($user_id != '') {
                $user_data = wp_update_user(array(
                    'ID' => $user_id,
                    'user_login' => rgar($entry, 1),
                    'user_email' => rgar($entry, 4),
                    'first_name' => rgar($entry, 2),
                    'last_name' => rgar($entry, 3),
                    'user_pass' => rgar($entry, 5)
                ));

                // update ticket number
                update_field( 'field_5c9ab3d788481', rgar($entry, 8), 'user_' . $user_id);

                // update table number
                update_field( 'field_5ca039a986009', rgar($entry, 10), 'user_' . $user_id );

                // update special menus
                $specialMenus = array(
                    'silla_de_ruedas' => rgar($entry, 11),
                    'ninos' => rgar($entry, 12),
                    'alergicos' => rgar($entry, 14),
                    'diabeticos' => rgar($entry, 15),
                    'vegetarianos' => rgar($entry, 16)
                );
                update_field( 'field_5ca3dbeb57a88', $specialMenus, 'user_' . $user_id );

                // update user total
                $numberTickets = (int) rgar($entry, 8);
                $eventCode = get_field('event_code', 'user_' . $user_id);
                $event = get_posts(array(
                    'numberposts'	=> -1,
                    'post_type'		=> 'event',
                    'meta_key'		=> 'codigo_del_evento',
                    'meta_value'	=> $eventCode
                ));
                $ticketPrice = (int) get_field('precio_del_boleto', $event[0]->ID);
                $specialMenu = get_field('menu_especial', 'user_' . $user_id);
                $kidMenus = (int) $specialMenu['ninos'];
                $discount = 100 * $kidMenus;
                $total = ($ticketPrice * $numberTickets) - $discount;
                update_field( 'field_5c9c1d91a5cc6', $total, 'user_' . $user_id);

                // update user remaining
                $amountPaid = 0;
                $payments = get_posts(array(
                    'numberposts' => -1,
                    'post_type' => 'payment',
                    'meta_query' => array(
                        array(
                            'key' => 'user_id',
                            'value' => $user_id
                        ),
                        array(
                            'key' => 'event_code',
                            'value' => $eventCode
                        )
                    )
                ));
                
                foreach($payments as $payment) {
                    $qty = (int) get_field('quantity', $payment->ID);
                    $amountPaid += $qty;
                }
                $remaining = $total - $amountPaid;
                update_field( 'field_5c9c1da4a5cc7', $remaining, 'user_' . $user_id);

                // update event balance
                $graduates = get_users(array(
                    'role' => 'graduate',
                    'meta_key' => 'event_code',
                    'meta_value' => $eventCode
                ));
                //$currentBalance = (int) get_field('balance', $event[0]->ID);
                $eventRemaining = 0;
                foreach($graduates as $graduate) {
                    $eventRemaining += get_user_meta($graduate->ID, 'remaining', true);
                }
                update_field( 'field_5c9c27aa997f0', $eventRemaining, $event[0]->ID);
            }
	
            // Delete entry
            GFAPI::delete_entry( $entry['id'] );

            // Redirect to event page
            wp_redirect( site_url('/event' . '/' . $event[0]->post_name) );
        }
    }
