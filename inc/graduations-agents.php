<?php
    // Populate edit agent with correct info
    add_filter( 'gform_field_value', 'graduations_populate_agent', 10, 3 );
    function graduations_populate_agent( $value, $field, $name ) {
        if(is_page('users/edit')) {
            $agent = get_user_by('id', $_GET['user_id']);
            $values = array(
                'agent_username' => $agent->user_login,
                'agent_email' => $agent->user_email,
                'agent_name' => $agent->user_firstname,
                'agent_lastname' => $agent->user_lastname
            );
            return isset( $values[ $name ] ) ? $values[ $name ] : $value;
        } elseif(is_page('settings')) {
            $user = wp_get_current_user();
            $values = array(
                'agent_username' => $user->user_login,
                'agent_email' => $user->user_email,
                'agent_name' => $user->user_firstname,
                'agent_lastname' => $user->user_lastname
            );
            return isset( $values[ $name ] ) ? $values[ $name ] : $value;
        } else {
            return $value;
        }
    }

    // Create sales agent
    add_action( 'gform_after_submission_8', 'graduations_create_sales_agent', 10, 2 );
    function graduations_create_sales_agent($entry, $form) {
        if(is_page('users/edit') && current_user_can('app_admin')) {
            $user_id = (int) $_GET['user_id'];
            if($user_id != '') {
                $user_data = wp_update_user(array(
                    'ID' => $user_id,
                    'user_login' => rgar($entry, 2),
                    'user_email' => rgar($entry, 3),
                    'first_name' => rgar($entry, 1),
                    'last_name' => rgar($entry, 4),
                    'user_pass' => rgar($entry, 6)
                ));
            }
        } elseif(is_page('settings')) {
            $user = wp_get_current_user();
            $user_id = (int) $user->ID;
            if($user_id != '') {
                $user_data = wp_update_user(array(
                    'ID' => $user_id,
                    'user_login' => rgar($entry, 2),
                    'user_email' => rgar($entry, 3),
                    'first_name' => rgar($entry, 1),
                    'last_name' => rgar($entry, 4),
                    'user_pass' => rgar($entry, 6)
                ));
            }
        } else {
            $new_agent = wp_insert_user(array(
                'user_login' => rgar($entry, 2),
                'user_email' => rgar($entry, 3),
                'first_name' => rgar($entry, 1),
                'last_name' => rgar($entry, 4),
                'user_pass' => rgar($entry, 6),
                'role' => 'sales_agent'
            ));
        }

        // Delete entry
        GFAPI::delete_entry( $entry['id'] );
        if(is_page('settings')) {
            wp_redirect( site_url('/events') );
        } else {
            wp_redirect( site_url('/users') );
        }
    }

    // Delete sales agent
    add_action( 'wp_ajax_delete_user_action', 'graduations_delete_agent' );
    add_action( 'wp_ajax_nopriv_delete_user_action', 'graduations_delete_agent' );
    function graduations_delete_agent(){
        if(current_user_can('delete_users')) {
            check_ajax_referer( 'security-special-string', 'security' );
            wp_delete_user( $_POST['user_id'] );
            die();
        }
    }