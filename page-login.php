<?php
/**
 * Template Name: Login
 */
get_header(); ?>
    <div class="p-login">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 mx-auto">
                    <?php gravity_form( 1, false, false, false, false ); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>